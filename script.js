function gv (val) {
    if (val < 10) return '0'+val;
    else return val;
}
  
function refresh () {
    body = document.getElementsByTagName('body')[0]

    date = new Date();

    hex = '#'+gv(date.getHours())+gv(date.getMinutes())+gv(date.getSeconds())

    body.style.backgroundColor = hex

    input = document.getElementById('time')
    input.innerText = gv(date.getHours())+":"+gv(date.getMinutes())+":"+gv(date.getSeconds())
}

refresh()
setInterval(refresh, 1000)